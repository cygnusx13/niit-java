public class Lab1Task5{
	public static void main(String args[]){
		char[]userLine=args[0].toCharArray();
		digits myDig=new digits();
		myDig.printIt(userLine);
	}
}

class digits{
char[][] zero=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*','*','*',' '}
};
char[][] one=new char[][]{
{' ',' ','*','*',' '},
{' ','*',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '}
};
char[][] two=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ','*','*',' '},
{' ','*',' ',' ',' '},
{' ','*','*','*',' '}
};
char[][] three=new char[][]{
{' ','*','*','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ','*',' ',' '},
{' ',' ',' ','*',' '},
{' ','*','*','*',' '}
};
char[][] four=new char[][]{
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*','*','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '}
};
char[][] five=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ',' ',' '},
{' ','*','*','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '},
{' ','*','*','*',' '}
};
char[][] six=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ',' ',' '},
{' ','*',' ',' ',' '},
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ','*','*','*',' '}
};
char[][] seven=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ',' ',' ','*',' '},
{' ',' ','*',' ',' '},
{' ',' ','*',' ',' '},
{' ',' ','*',' ',' '}
};
char[][] eight=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ',' ','*',' ',' '},
{' ','*',' ','*',' '},
{' ','*',' ','*',' '},
{' ','*','*','*',' '}
};
char[][] nine=new char[][]{
{' ','*','*','*',' '},
{' ','*',' ','*',' '},
{' ','*','*','*',' '},
{' ',' ',' ','*',' '},
{' ',' ',' ','*',' '},
{' ','*','*','*',' '}
};

	public void printIt(char[] forPrint){
		int i=0;
		while(i<6){
			for(char bufChar: forPrint){
					if(bufChar=='0')
						System.out.print(zero[i]);
					if(bufChar=='1')
						System.out.print(one[i]);
					if(bufChar=='2')
						System.out.print(two[i]);
					if(bufChar=='3')
						System.out.print(three[i]);
					if(bufChar=='4')
						System.out.print(four[i]);
					if(bufChar=='5')
						System.out.print(five[i]);
					if(bufChar=='6')
						System.out.print(six[i]);
					if(bufChar=='7')
						System.out.print(seven[i]);
					if(bufChar=='8')
						System.out.print(eight[i]);
					if(bufChar=='9')
						System.out.println(nine[i]);					
			}
		System.out.println(' ');
		i++;
		}
			
	}
}