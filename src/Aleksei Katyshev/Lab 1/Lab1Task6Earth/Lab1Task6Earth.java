import java.math.BigDecimal;

public class Lab1Task6Earth{
	public static void main(String[] args){
	double firstRadius=6378.1;
	Circles myCircle=new Circles();
	System.out.println("Before:");
	myCircle.setRadius(firstRadius);
	myCircle.printIt();
	myCircle.setFerence(myCircle.getFerence()+1);
	myCircle.setArea(0);
	myCircle.setRadius(0);
	System.out.println("After:");
	myCircle.printIt();
	System.out.printf("Gap = %.2f M\n",(myCircle.getRadius()-firstRadius));
	}
}

class Circles{
	private double radius=0;	//= ference/2*Pi = корень из area/Pi
	private double ference=0; //окружность =2*Pi*radius
	private double area=0;	//площадь =Pi*(radius*radius)
	
	public void setRadius(double myRad){
		radius=myRad;
	}
	public void setFerence(double myFer){
		ference=myFer;
	}
	public void setArea(double myArea){
		area=myArea;
	}
	
	public double getRadius(){
		return radius;
	}
	public double getFerence(){
		return ference;
	}
	public double getArea(){
		return area;
	}
	
	private boolean checkComponents(){
		if (radius==0){
			if (ference!=0)
				radius=ference/(2*3.14);
			else if (area!=0)
				radius=Math.sqrt(area/3.14); 
			else {
				System.out.println("Error 1: no settings!");
				return(false);
			}
		}
		if (ference==0)
			ference=2*3.14*radius;
		if(area==0)
			area=3.14*Math.pow(radius,2);
		return true;
	}
	
	public void printIt(){
		if(checkComponents()){
		
			BigDecimal radiusBD=new BigDecimal(radius);
			System.out.println("Radius= "+radiusBD.setScale(2,BigDecimal.ROUND_HALF_UP));
			BigDecimal ferenceBD=new BigDecimal(ference);
			System.out.println("Ference= "+ferenceBD.setScale(2,BigDecimal.ROUND_HALF_UP));
			BigDecimal areaBD=new BigDecimal(area);
			System.out.println("Area= "+areaBD.setScale(2,BigDecimal.ROUND_HALF_UP));

		}
		else
			System.out.println("Check Components = false");
	}
	

}


