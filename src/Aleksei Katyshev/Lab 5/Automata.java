
public class Automata{
    private double balance=0;

    enum allStatus{
        OFF,WAIT,ACCEPT,COOK,CHANGE
    }
    allStatus status=allStatus.OFF;

    //===========================================Method
    public void setBalance(String cash){
        balance+=Integer.parseInt(cash);
    }
    public double getBalance(){
        return balance;
    }

    public String getStatus(){
        return status.toString();
    }
    public void setStatus(String input){
        status=allStatus.valueOf(input);
    }

    public boolean check(double input){
        if(input>balance)
            return false;
        else
            return true;
    }

    public void choice(double input){
            balance-=input;
   }

    public double giveChange(){
        double change=0;
        if(balance>0){
            change=balance;
            balance=0;
            return change;
        }
        return change;
    }


}//end Automata