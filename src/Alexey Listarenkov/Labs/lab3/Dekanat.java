import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.*;
import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

@XmlRootElement
public class Dekanat {
    private static String groupsFile = "groups.txt";
    private static String studentsFile = "students.txt";
    private static String xmlFile = "dekanat.xml";

    private ArrayList<Group> groups = new ArrayList<>();
    private ArrayList<Student> students = new ArrayList<>();

    public ArrayList<Group> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<Group> groups) {
        this.groups = groups;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    // создать студента
    public Student createNewStudent(String studentFIO) {
        Student student = new Student();
        student.setFio(studentFIO);
        students.add(student);
        return student;
    }

    //создать группу
    public void createNewGroup(String groupTitle) {
        Group group = new Group();
        group.setTitle(groupTitle);
        groups.add(group);
    }

    // поиск студента
    public Student searchStudent(String parameter) {
        for (Student st : students) {
            if (st.getId().equals(parameter) || (st.getFio()).contains(parameter))
                return st;
        }
        return null;
    }

    // поиск группы по названию
    public Group searchGroup(String title) {
        for (Group gr : this.getGroups()) {
            if (gr.getTitle().equalsIgnoreCase(title))
                return gr;
        }
        return null;
    }

    // перевести студента в другую группу
    public void moveStudentToGroup(Student student, Group newGroup) {
        Group oldGroup = this.searchGroup(student.getGroup());
        if (oldGroup != null) {
            oldGroup.getStudents().remove(student);
            oldGroup.setNum();
        }
        newGroup.getStudents().add(student);
        student.setGroup(newGroup);
        newGroup.setNum();
    }

    //удалить студента
    public void deleteStudent(String id) {
        Student student = searchStudent(id);
        String title = student.getGroup();
        Group studentsGroup = searchGroup(title);
        int index = groups.indexOf(studentsGroup);

        students.remove(student);
        groups.get(index).getStudents().remove(student);
        studentsGroup.setNum();
    }

    //генерация оценок студентам
    public void generateMarks() {
        for (Student st : getStudents()) {
            ArrayList<Integer> marks = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                marks.add(2 + (int) (Math.random() * 4));
            }
            st.setMarks(marks);
            st.setAverageScore();
        }
    }

    // отчислить студента за неуспеваемость
    public void expelStudent() {
        ArrayList<String> expelStudentID = new ArrayList<>();
        for (Student st : this.students) {
            if (st.getAverageScore() < 3) {
                expelStudentID.add(st.getId());
                System.out.println("За неуспеваемость отчислен студент :\n" +
                        "ID " + st.getId() + " " + " ФИО " + st.getFio() + " " + " Группа " + st.getGroup() +
                        " Оценки " + st.getMarks() + " Средний балл: " + st.getAverageScore());
            }
        }
        for (String id : expelStudentID)
            deleteStudent(id);
    }

    //инициализация выборов старост групп
    public void startElection() {
        for (int i = 0; i < groups.size(); i++)
            groups.get(i).setHead();
    }

    //возвращает значение id на 1 больше максимального id
    public String searchMaxID() throws NumberFormatException {
        Integer maxID = 0;
        for (Student st : students) {
            int id = Integer.parseInt(st.getId());
            if (id > maxID)
                maxID = id;
        }
        maxID++;
        return maxID.toString();
    }

    // вывод на консоль данных о студентах
    public void describeStudent() {
        System.out.println("Список студентов\n");
        System.out.println("Количество Студентов " + (students.size()));
        for (Student s : students) {
            if (s.getGroup() == null)
                System.out.println("ID " + s.getId() + " \t" + " ФИО " + s.getFio());
            else
                System.out.println("ID " + s.getId() + " \t" + " ФИО " + s.getFio() + " \t" + " Группа " + s.getGroup() + " \t" + " Оценки " + s.getMarks() + " \t" + " Средний балл: " + s.getAverageScore());
        }
    }

    //     вывод на консоль данных о группах
    public void describeGroup() {
        System.out.println("\nКоличество групп " + (groups.size()));
        System.out.println("Список групп\n");

        for (Group g : groups) {
            if (!g.getTitle().equals("без группы") && g.getStudents().size() != 0) {
                DecimalFormat format = new DecimalFormat("#.##");
                String avGroupScore = format.format(g.calculateAvScore());
                System.out.println("\nГруппа " + g.getTitle() + " Количество студентов: " + g.getNum() + " Средний балл группы: " + avGroupScore);
                if (g.getHead() != null)
                    System.out.println(" Староста: " + g.getHead().getFio());
            } else {
                System.out.println(g.getTitle());
            }
            System.out.println();
            for (Student s : g.getStudents())
                System.out.println("ID " + s.getId() + " \t" + " ФИО " + s.getFio() + " \t" + " Оценки " + s.getMarks() + " \t" + " Средний балл: " + s.getAverageScore());
        }

    }


    // загружает списки студентов и групп из исходных файлов
    // создаёт базовую структуру - есть студенты без id и они не распределены по группам
    public void createDekanat() {
        ArrayList<Group> groups = new ArrayList<>();
        ArrayList<Student> students = new ArrayList<>();

        String[] st = readInitialFile(studentsFile);
        for (String s : st) {
            Student student = new Student();
            student.setFio(s);
            students.add(student);
        }
        String[] gr = readInitialFile(groupsFile);
        for (String s : gr) {
            Group group = new Group();
            group.setTitle(s);
            groups.add(group);
        }
        this.setGroups(groups);
        this.setStudents(students);
    }

    // читает исходный текстовый файл, возвращает массив строк , прочитанных из файла
    private String[] readInitialFile(String file) {
        ArrayList<String> str = new ArrayList<>();
        try {
            FileReader reader = new FileReader(file);
            BufferedReader br = new BufferedReader(reader);
            String line;
            int i = 0;
            while ((line = br.readLine()) != null) {
                str.add(i, line);
                i++;
            }
            reader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] strArr = new String[str.size()];

        return str.toArray(strArr);
    }

    // генерация XML из объекта "Dekanat" и запись в файл
    public void marshall() {
        File outFile = new File(xmlFile);
        try {
            JAXBContext context = JAXBContext.newInstance(Dekanat.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            marshaller.marshal(this, System.out); // вывод на экран
            marshaller.marshal(this, outFile); // вывод в файл

            System.out.println("\nФайл " + outFile.toString() + " сгенерирован");
        } catch (JAXBException exception) {
            Logger.getLogger(Dekanat.class.getName()).log(Level.SEVERE, "marshall threw JAXBException", exception);
        }
    }

    // загружает объект из xml файла
    public Object unmarshal() throws JAXBException {
        File inFile = new File(xmlFile);
        Object object = new Object();
        try {
            JAXBContext context = JAXBContext.newInstance(Dekanat.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            object = unmarshaller.unmarshal(inFile);

            System.out.println("Objects created from XML:");
            System.out.println();
        } catch (JAXBException exception) {
            Logger.getLogger(Dekanat.class.getName()).log(Level.SEVERE, "marshall threw JAXBException", exception);
        }
        return object;
    }
}
