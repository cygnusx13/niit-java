package kostya;

//проектный менеджер.
public class ProjectManager extends Manager implements Heading{
    private int numSubordinates = 0;         //количество подчиненых

    public ProjectManager(int id, String name, String position, int projectCost, int numManagersInProject, int numSubordinates){
        super(id, name, position, projectCost, numManagersInProject);
        this.numSubordinates = numSubordinates;
    }

    //получение количества подчиненных
    public int getNumSubordinates(){
        return numSubordinates;
    }

    //расчет оплаты исходя из участия в проекте (проектный менеджер получает 30% от стоимости проекта минус 10%, выделенных на менеджеров (если они есть))
    @Override
    public double calcPaymentForProject() {
        double paymentForProject;
        if (getNumManagers()>0) paymentForProject = (getProjectCost() * 0.2);
        else paymentForProject = getProjectCost() * 0.3;
        return paymentForProject;
    }

    //расчет оплаты исходя из руководства (количество подчиненных умножается на коэффициент "руководства")
    @Override
    public double calcPaymentForHeading() {
        return numSubordinates * 10000;
    }

    //расчет зароботной платы
    @Override
    public double calcPayment() {
        setPayment(calcPaymentForProject() + calcPaymentForHeading());
        return getPayment();
    }

}
