
public class RangeShort{
	public static void main(String[] args){
		
		String str = args[0];
		String[] arrStr = str.split(",");	//разбиваем полученную на входе строку на массив строк arrStr
		int[] arrInt = new int[arrStr.length];	//создаем массив целых чисел arrInt
		
		//приводим массив строк к массиву целых чисел
		for(int k=0; k<arrStr.length; k++)
		{
			arrInt[k] = Integer.parseInt(arrStr[k]);
		}
		
		int rangeLeft=0;	//левая граница диапазона (который нужно свернуть)
		int rangeRight=0;	//правая граница диапазона (который нужно свернуть)
		
		for(int i=0; i<arrStr.length; i++)
		{
			if(i!=arrStr.length-1)	//если текущий элемент массива не последний
			{	 
				if(arrInt[i+1]!=arrInt[i]+1)	//если следующий элемент массива не равен текущему + 1
				{
					System.out.print(arrInt[i]+",");	//выводим текущий элемент массива + ","
				}
				else if(arrInt[i+1]==arrInt[i]+1)	//если следующий элемент массива равен текущему + 1
				{
					rangeLeft=arrInt[i];	//левая граница диапазона равна текущему элементу массива
					int countRange=0;	//счетчик количества элементов в диапазоне
					
					//перебираем элементы массива, начиная с i-го, для нахождения правой границы массива
					for(int j=i; j<arrStr.length-1; j++)
					{
						if(arrInt[j+1]==arrInt[j]+1)	//если следующий элемент массива равен текущему + 1
						{
							rangeRight=arrInt[j+1];		//правая граница диапазона равна текущему элементу массива
							i++;						//переводим счетчик во внешнем цикле на следующий элемент
							countRange++;					//увеличиваем счетчик количества элементов в диапазоне на 1
						}
						else if(arrInt[j+1]!=arrInt[j]+1)	//если следующий элемент массива не равен текущему + 1
						{
							break;	//выходим из цикла, 
						}
					}
					
					if(countRange==1) System.out.print(rangeLeft+","+rangeRight);	//если в диапазоне 2 элемента, выводим левую и правую границу через ","
					else System.out.print(rangeLeft+"-"+rangeRight);	//если в диапазоне больше 2 элементов, выводим левую и правую границу через "-"
					
					if(i!=arrStr.length-1) System.out.print(",");	//если текущий элемент массива не последний, выводим ","
				}
			}
			else if(i==arrStr.length-1)	System.out.print(arrInt[i]); //если текущий элемент массива последний, выводим его без ","
		}	
	}
}	

