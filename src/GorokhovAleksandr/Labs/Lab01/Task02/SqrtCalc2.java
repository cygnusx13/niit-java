import java.util.Scanner;

class Sqrt
{
   //double delta=0.00000001;
   double arg;

   Sqrt(double arg) {
      this.arg=arg;
   }
   double average(double x,double y) {
      return (x+y)/2.0;
   }
   boolean good(double guess,double x, double delta) {
      return Math.abs(guess*guess-x)<delta;
   }
   double improve(double guess,double x) {
      return average(guess,x/guess);
   }
   double iter(double guess, double x, double delta) {
      if(good(guess,x, delta))
         return guess;
      else
         return iter(improve(guess,x),x, delta);
   }
   public double calc(double delta) {
      return iter(1.0,arg, delta);
   }
}

class SqrtCalc2
{
	public static void main(String[] args)
	{
	Scanner in = new Scanner(System.in);
    System.out.print("Value: ");	
	double val = in.nextDouble();
	System.out.print("Delta: ");
	double delta = in.nextDouble();	
    Sqrt sqrt=new Sqrt(val);
    double result=sqrt.calc(delta);
    System.out.format("Sqrt of %f=%.5f", val,result);
	//System.out.format "зазор: %.3f метра", 
    }
}