/*
Lab1_6.
Разработать класс Circle.
При помощи класса Circle решить две вспомогательные задачи: "Земля и верёвка", "Бассейн".
*/
class Circle{
	
	private double radius;
	private double ference;
	private double area;
	
	public void setRadius(double r){
		radius = r;
		ference = 2*Math.PI*radius;
		area = Math.PI*radius*radius;		
	}
	public void setFerence(double f){
		ference = f;
		radius = ference / (2*Math.PI);
		area = Math.PI*radius*radius;
	}
	public void setArea(double a){

		area = a;
		radius = Math.sqrt((area/Math.PI));
		ference = 2*Math.PI*radius;
	}	
	public double getRadius(){
		return radius;
	}
	public double getFerence(){
		return ference;
	}
	public double getArea(){
		return area;
	}
}

public class Lab1_6{
	
	// в метрах
	static final double radiusEarth = 6378100.0; 
	static final double insert = 1.0;            
	
	static final double radiusPool = 3.0;
	static final double widthTrack = 1.0;
	static final double priceFence = 2000.0;
	static final double priceTrack = 1000.0;
	
	public static void main(String[] args){		
		Circle one = new Circle();    
	//Земля и верёвка			
		one.setRadius(radiusEarth);		
		one.setFerence(one.getFerence() + insert);        		
		System.out.printf("Clearance: %.7f m \n", one.getRadius() - radiusEarth);	
	//Бассейн
		double t_priceTrack, t_priceFence, buf_area;
		one.setRadius(radiusPool);
		t_priceTrack = one.getFerence() * priceFence;
		buf_area = one.getArea();
		one.setRadius(radiusPool + widthTrack);
		t_priceFence = (one.getArea() - buf_area) * priceTrack;	
		System.out.printf("Total price: %.1f $\n", t_priceTrack + t_priceFence);	
	}	
}