/*
Lab1_1. 
Программа, которая находит наибольшую последовательность Коллатца для чисел в диапазоне от 1 до 1 000 000.
*/

public class Lab1_1{

    public static int counter = 0;

    public static int collatz(long n){
        counter ++;
        if (n == 1) return counter;
        else if (n%2 == 0) return collatz(n/2);
        else return collatz(3*n+1);
    }

    public static void main(String[] args){
        int buf, max_length = 0, max_n = 0;
        for (int i = 1; i <= 1000000; i++) {
            counter = 0;
            buf = collatz(i);
            if(buf > max_length) {
                max_length = buf;
                max_n = i;
            }
        }
        System.out.println("Collatz lenght = " + max_length + ", max_N = "+ max_n);
    }
}

