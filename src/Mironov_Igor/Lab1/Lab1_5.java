/*
Lab1_5.
Напишите программу, которая будет считывать из командной строки число и выводить его в таком виде чисел из звездочек.
*/
import java.util.Scanner;

public class Lab1_5{
	
	static void toStars (String n){
		
		final int SIZE = 7;
		String starNambers[][] = {        
    {"  ***  ",   
     " *   * ",
     " *   * ",
     " *   * ",
     " *   * ",
     " *   * ",
     "  ***  "},
        
    {"   *   ",
     "  **   ",
     " * *   ",
     "   *   ",
     "   *   ",
     "   *   ",
     " ***** "},
         
    {"  ***  ",
     " *   * ",
     "    *  ",
     "   *   ",
     "  *    ",
     " *     ",
     " ***** "},
                    
    {" ***** ",
     "    *  ",
     "   *   ",
     "  **** ",
     "    *  ",
     "   *   ",
     "  *    "},
                    
    {" *   * ",
     " *   * ",
     " *   * ",
     " ***** ",
     "     * ",
     "     * ",
     "     * "},
                    
    {" ***** ",
     " *     ",
     " *     ",
     " ***** ",
     "     * ",
     "     * ",
     " ***** "},
                    
    {"    *  ",
     "   *   ",
     "  *    ",
     " ***** ",
     " *   * ",
     " *   * ",
     " ***** "},
                    
    {" ***** ",
     "     * ",
     "     * ",
     "    *  ",
     "   *   ",
     "  *    ",
     " *     "},
                    
    {" ***** ",
     " *   * ",
     " *   * ",
     " ***** ",
     " *   * ",
     " *   * ",
     " ***** "},
                    
    {" ***** ",
     " *   * ",
     " *   * ",
     " ***** ",
     "    *  ",
     "   *   ",
     "  *    "}
    };
		
		char[] number = n.toCharArray();		
		String[][] out = new String [SIZE][number.length];
		
		for(int i = 0; i < number.length; i++)	
			for(int j = 0; j < SIZE; j++)
				out[j][i] = starNambers[Character.getNumericValue(number[i])][j];				
				
        for(int i = 0; i < SIZE; i++)	{	
			for(int j = 0; j < number.length; j++) 
				System.out.print(out[i][j]);			
			System.out.print('\n');	
		}			
	}
	
	public static void main(String[] arg){
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the number: ");
		toStars(in.nextLine());
	}
}