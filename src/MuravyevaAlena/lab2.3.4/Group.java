package decanat;

import java.util.Arrays;

enum Performance{BAD,GOOD};

public class Group{
   static final int MAX_STUDENTS_IN_GROUP = 50;
   static final int FROM=0;
   static final int VARIABLES=1;
   private String title;
   private Student []students = new Student[MAX_STUDENTS_IN_GROUP];
   private int num;
   public Student head;
   public int totalAverage=0;
   public Performance performance=Performance.GOOD;
   
   public Group(){
   }
   public Group(String t){
      title=t;
   }
   public String getName(){
      return title;
   }
   public void printStudents(){
      System.out.println(title);
      for(int i=0;i<num;i++){
         students[i].printStudent();
      }
   }
   public void electionHead(){
      int to=num;
      int randomNumber=0;
      randomNumber= FROM+(int) (Math.random()*to);
      head=students[randomNumber];
   }
   public void searchStudent(char []arrEnterUser){
      for(int i=0;i<num;i++)
      if(Arrays.equals(arrEnterUser,students[i].getFio())){
         String stringName = new String(arrEnterUser);
         System.out.printf("Student found: name %s , group %s",stringName,getName());
      }
   }
   public void calculationAveregeBall(){
      for(int i=0;i<num;i++){
         totalAverage=totalAverage + (students[i].getAverage());
      }
   }
   public int getTotalAverage(){
      return totalAverage/num;
   }
   public int getNum(){
      return num;
   }
   public void deletionStudent(){
      for(int i=0;i<num;i++){
         if(students[i].performance==Performance.BAD){
            students[i]=students[num-VARIABLES];
            students[num-VARIABLES]=null;
            num--;
         }
      }
   }
   public void delStudent(int randomStudent1){
      students[randomStudent1]=null;
      students[randomStudent1]=students[num-VARIABLES];
      students[num-VARIABLES]=null;
      num--;
   }
   public Student getStudentByIndex(int index){
      return students[index];
   }
   public void addStudent(Student tmpStudent){
      for(int i=0; i<students.length;i++)
      {
         if(students[i] == null)
         {
            students[i] = tmpStudent;
            num++;
            return;
         }
      }
      if(num >= MAX_STUDENTS_IN_GROUP)
      {
         System.out.println("Can't add new student. Maximum students in group is exceeded.");
         return;
      }
      if(students[num] == null)
      {
         students[num] = tmpStudent;
         num++;
         return;
      }
      else{
         System.out.println("Error. Another student in the place");
         return;
      }
   }
}
