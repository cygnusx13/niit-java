package staffDemo;

class Manager extends Employee implements Project{
   StaffDemo astaffDemo=new StaffDemo();
   public double bonus=0;
   
   public Manager(int aid,String aname,String aposition,int aworktime){
      super(aid,aname,aposition,aworktime);
   }
   public Manager(double apayment){
      super(apayment);
   }
   public void bonus(int budgetProject, double coefficient){
      bonus = budgetProject*coefficient;
   }
   @Override
   public void calculationPayment(){
      payment=bonus;
   }
}
class ProjectManager extends Manager implements Heading{
   public double supplementForPeople=3000;
   public double headPay=0;
   public ProjectManager(int aid,String aname,String aposition,int aworktime){
      super(aid,aname,aposition,aworktime);
   }
   public ProjectManager(double apayment){
      super(apayment);
   }
   public void paymentForPeople(int headPeople) {
      headPay=supplementForPeople * headPeople;
   }
   @Override
   public void calculationPayment(){
      payment= bonus+headPay;
   }
}

class SeniorManager extends ProjectManager{
   public SeniorManager(int aid,String aname,String aposition,int aworktime){
      super(aid,aname,aposition,aworktime);
      supplementForPeople=7000;
   }
   public SeniorManager(double apayment){
      super(apayment);
   }
}
