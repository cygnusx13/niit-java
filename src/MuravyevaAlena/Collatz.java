
public class Collatz {
   static int MIN_VALUE=2;
   static int  MAX_VALUE=100000;
   static int FOR_THE_FORMULA=3;
   static long nextnumber = 0;
   static int longestSequenceCollapse=0;
   static int sequenceLen = 0;

   public static void main(String[] args) {
      Collatz cl=new Collatz();
      long sourseN=0;
       for ( long n=MAX_VALUE; n>1; n--)
       {
          sequenceLen = 0;
          cl.sequenceCollapse(n);
          if (longestSequenceCollapse < sequenceLen)
          {
             longestSequenceCollapse = sequenceLen;
             sourseN=n;
          }
       }
       System.out.printf("The longest sequence of collapse at n=%d equal %d\n",sourseN,longestSequenceCollapse);
   }
   int sequenceCollapse(long  n)
   {
      if (n==1) return 1;
      if (n%MIN_VALUE==0)
      {
         nextnumber=n/MIN_VALUE;
         sequenceLen++;
         sequenceCollapse(nextnumber);
      }
      else
      {
         sequenceLen++;
         nextnumber=FOR_THE_FORMULA*n+1;
         sequenceCollapse(nextnumber);
      }
      return 1;
   }
}

   


