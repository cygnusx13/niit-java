/*
Задача 2. Квадратный корень числа.
Реализовать алгоритм вычисления квадратного корня. Взять за основу пример
программы. Изменить пример так, чтобы была возможность регулирования
точности расчетов.
Точность занимает второй аргумент.
 */

public class Sqrt {

    double delta = 0.00000001;
    double arg;

    Sqrt(double arg) {
        this.arg = arg;
    }

    double average(double x, double y) {
        return (x + y) / 2.0;
    }

    boolean good(double guess, double x) {
        return Math.abs(guess - x) < x * delta;
    }

    double improve(double guess, double x) {
        return average(guess, x / guess);
    }

    double iter(double guess, double imp, double x) {
        if (good(guess, imp)) {
            return guess;
        } else {
            return iter(improve(guess, x), guess, x);
        }
    }

    public double calc(double d) {
        delta = d;
        return iter(1.0, 1.2, arg);
    }
}

class Program {

    public static void main(String[] args) {
        double val = Double.parseDouble(args[0]);
        double w = Double.parseDouble(args[1]);
        Sqrt sqrt = new Sqrt(val);
        double result = sqrt.calc(w);
        System.out.println("Sqrt of " + val + " = " + result);
    }
}
