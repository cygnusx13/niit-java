
public class TeamLeader extends Programmer implements Heading {

    private int priceOfEmp;

    public TeamLeader(int id, String name, int workTime, int payment, int percentages, int priceOfEmp) {
        super(id, name, workTime, payment, percentages);
        this.priceOfEmp = priceOfEmp;
    }

    public int calcPaymentForHeading() {
        return workProject.getNumOfEmployees() * priceOfEmp;
    }

    @Override
    public int calcPayment() {
        return super.calcPayment() + calcPaymentForHeading();
    }
}
