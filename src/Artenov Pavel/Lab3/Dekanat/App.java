package Dekanat2;

/*
Проект DekanatDemo
Создать два файла с данными для групп и студентов(не менее 3 групп и
30 студентов).Использовать эти файлы при формировании данных групп и
студентов.
Написать демонстрационную версию приложения.
*/


public class App
{
    public static void main (String [] args) {
        Dekanat dekanat = new Dekanat();
        dekanat.addStudent("Student");
        dekanat.addGroup("Group");
        dekanat.bindStudentToGroup();
        dekanat.bindGroupToStudent();
        dekanat.addRandomMark();
        dekanat.marksStatistic();
        dekanat.selHead("Math");
        dekanat.findStudent("KalyaevNN");
        dekanat.findStudent(25);
        dekanat.avgMarkInGroup("Eng");
        dekanat.deleteStudent(22);
        dekanat.changeGroup(15,"Bio");
        dekanat.printDataP();
        dekanat.writeResult("result");

    }

}