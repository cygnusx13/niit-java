import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.time.LocalTime;

public class Main extends Application {

    static int hourToGrad(int hour, int min)
    {
        //System.out.println(hour * 360 / 12);
        hour %= 12;
        return hour * 360 / 12 + min * 360 / 12 / 60 ;
    }

    static int minToGrad(int min, int sec)
    {
        //System.out.println(min * 360 / 60);
        return min * 360 / 60 + sec * 360 / 60 / 60;
    }

    static int secToGrad(int sec)
    {
        return sec * 360 / 60;
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        StackPane sp = new StackPane();


        //ImageView imClockFace =
        ImageView ivClockFace = new ImageView(new Image("/clockFace.png"));

        Image imHourArrow = new Image("/hourArrow.png");
        Node nodeHourArrow = new ImageView(imHourArrow);

        Image imMinuteArrow = new Image("/minuteArrow.png");
        Node nodeMinuteArrow = new ImageView(imMinuteArrow);

        Image imSecondArrow = new Image("/secondArrow.png");
        Node nodeSecondArrow = new ImageView(imSecondArrow);

        sp.getChildren().add(nodeHourArrow);
        sp.getChildren().add(nodeMinuteArrow);
        sp.getChildren().add(nodeSecondArrow);
        sp.getChildren().add(ivClockFace);


        sp.getChildren().get(0).setTranslateY(imHourArrow.getHeight() / 2 - 20);//HourArrow
        sp.getChildren().get(1).setTranslateY(imMinuteArrow.getHeight() / 2 - imMinuteArrow.getWidth() / 2);//MinArrow
        sp.getChildren().get(2).setTranslateY(imSecondArrow.getHeight() / 2 - 65);//SecArrow

        LocalTime lt = LocalTime.now();

        int startAngleHourArrow = hourToGrad(lt.getHour(), lt.getMinute()) + 180;
        Rotate rotateHourArrow = new Rotate(startAngleHourArrow, imHourArrow.getWidth() / 2,20);
        nodeHourArrow.getTransforms().add(rotateHourArrow);

        int startAngleMinuteArrow = minToGrad(lt.getMinute(), lt.getSecond()) + 180;
        Rotate rotateMinuteArrow = new Rotate(startAngleMinuteArrow, imMinuteArrow.getWidth() / 2,imMinuteArrow.getWidth() / 2);
        nodeMinuteArrow.getTransforms().add(rotateMinuteArrow);

        int startAngleSecondArrow = secToGrad(lt.getSecond()) + 180;
        Rotate rotateSecondArrow = new Rotate(startAngleSecondArrow, imSecondArrow.getWidth() / 2,65);
        nodeSecondArrow.getTransforms().add(rotateSecondArrow);



        Timeline tlHour = new Timeline();
        tlHour.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.seconds(60 * 60 * 12),
                        new KeyValue(rotateHourArrow.angleProperty(), startAngleHourArrow + 360)
                )
        );


        Timeline tlMin = new Timeline();
        tlMin.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.seconds(60 * 60),
                        new KeyValue(rotateMinuteArrow.angleProperty(), startAngleMinuteArrow + 360)
                )
        );



        Timeline tlSec = new Timeline();
        tlSec.getKeyFrames().addAll(
                new KeyFrame(
                        Duration.millis(60000),
                        new KeyValue(rotateSecondArrow.angleProperty(), startAngleSecondArrow + 360)
                )
        );

        tlHour.setCycleCount(Animation.INDEFINITE);
        tlMin.setCycleCount(Animation.INDEFINITE);
        tlSec.setCycleCount(Animation.INDEFINITE);

        tlHour.play();
        tlMin.play();
        tlSec.play();

        primaryStage.setScene(new Scene(sp, 950, 950));
        primaryStage.setResizable(false);
        primaryStage.centerOnScreen();
        primaryStage.show();

    }


    public static void main(String[] args) {
        launch(args);
    }
}