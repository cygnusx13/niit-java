package mypackage;
/**
 * Created by me on 4/9/17.
 */


class Student
{
    private int iD;
    private String fio;
    private int marks[] = new int[10];
    private Group group = null;

    Student(int id, String fio)
    {
        this.iD = id;
        this.fio = fio;
    }

    void toGroup(Group group)
    {
        this.group = group;
    }

    int getID()
    {
        return this.iD;
    }

    String getFIO()
    {
        return this.fio;
    }

    Group getGroup()
    {
        return group;
    }

    void addMark(int mark)
    {
        for(int i = 0; i < marks.length; i++)
            if(marks[i] == 0)
            {
                marks[i] = mark;
                return;
            }
    }

    float calcAverageMark()
    {
        float sumMarks = 0.0f;
        float numMarks = 0.0f;
        for(int i = 0; i < marks.length; i++)
            if(marks[i] != 0)
            {
                sumMarks += marks[i];
                numMarks++ ;
            }
        return sumMarks / numMarks;
    }
}