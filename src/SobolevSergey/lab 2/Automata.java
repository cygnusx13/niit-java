package Automata;

public class Automata
{
    // state - текущее состояние автомата;
    enum STATES { OFF, ON, WAIT, ACCEPT, CHECK, COOK }
    public STATES state;

    // cash - для хранения текущей суммы;
    int cash;

    // конструктор
    // on() - включение автомата;
    public Automata () {
        this.cash = 0;
        this.state = STATES.OFF;
        printState();
        Power(true);
        printState();
        Wait(true);
        printState();
    }

    // coin() - занесение денег на счёт пользователем;
    public int coin(int cash){
        if (state == STATES.WAIT){
            this.state = STATES.ACCEPT;
            printState();
            return this.cash = cash;
        }else{
            this.state = STATES.OFF;
            return 0;
        }
    }

    // prices - массив цен напитков (соответствует массиву menu);
    int[] prices = new int[] {12,14,14,24,22,8,12,8,20,10,1,1};

    // printMenu() - отображение меню с напитками и ценами для пользователя;
    String[] menu = new String[] {"Эспрессо         ","Американо        ","Капучино         ","Мокачино         ",
            "Горячий шоколад  ",		"Чай              ","Бульон           ","Холодный чай     ","Кофе глиссе      ",
            "Молочный коктейль",		"Молоко           ","Лимон            "};
    public void printMenu(){
        if (state == STATES.ACCEPT){
            int i=0;
            System.out.println("\n\tМеню\n");
            while(i<12){
                System.out.println((i+1) + "\t" + menu[i] + "\t" + prices[i]);
                i++;
            }
        }
    }

    int menuItem;
    public int choice(int indexItem){
        if (state == STATES.ACCEPT){
            this.state = STATES.CHECK;
            this.menuItem = indexItem;
            System.out.println("\nВыберите элемент меню:");
            System.out.println("\nВы выбрали пункт: " + (menuItem + 1) + " " + menu[menuItem]);
            check(this.cash, menuItem);
            return menuItem;
        }else{
            this.state = STATES.OFF;
            return 0;
        }
    }

    // cancel() - отмена сеанса обслуживания пользователем;
    public void cancel (int cancelSession){
        if(cancelSession == 2){
            this.state = STATES.OFF;
            System.out.println("\nСеанс обслуживания завершен.");
        }
        else
            this.state = STATES.ACCEPT;
    }

    // check() - проверка наличия необходимой суммы;
    public int check(int balance, int menuItem){
        if(balance>=prices[menuItem]){
            Accept(true);
            System.out.println("Ваши деньги внесены в автомат");
            printState();
            Cook(true);
            printState();
            System.out.println("Готово.");
            System.out.println("Остаток средств: " + (cash - prices[menuItem]));
        }else{
            System.out.println("Денег не хватает");
            Wait(true);
            printState();
        }
        return balance;
    }

    public void Power(boolean powerOnOff){
        state = powerOnOff == true ? STATES.ON : STATES.OFF;
    }

    public void Wait(boolean wait){
        state = wait == true ? STATES.WAIT : STATES.OFF;
    }

    public void Accept(boolean accept){
        state = accept == true ? STATES.ACCEPT : STATES.OFF;
    }

    public void Cook(boolean cook){
        state = cook == true ? STATES.COOK : STATES.OFF;
    }

    // printState() - отображение текущего состояния для пользователя;
    public void printState(){
        switch (state){
            case OFF:
                System.out.println("Автомат выключен.");
                break;
            case ON:
                System.out.println("Автомат включен.");
                break;
            case WAIT:
                System.out.println("Ожидание...");
                break;
            case ACCEPT:
                System.out.println("Прием денег...");
                break;
            case CHECK:
                System.out.println("Проверка наличности...");
                break;
            case COOK:
                System.out.println("Приготовление...");
                break;
        }
    }
}
