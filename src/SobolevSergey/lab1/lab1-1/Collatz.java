/*  Лабораторная 1. Задача 1. Последовательность Коллатца
Найти наибольшую последовательность Коллатца для чисел в диапазоне от 1 до 1 000 000.

*/

public class Collatz {
	public static long collatz(long n) {
        if (n == 1) return 1;
        else if (n % 2 == 0) return (1+collatz(n / 2));
        else return (1+collatz(3*n + 1));
    }
	
	public static void print_collatz() {
       	int k=0;
		long max=0, count=0;
		for(int i=1;i<=1000000;i++){
		    count=collatz(i);
			if (count>max){
				max=count;
				k=i;
			}
		}
		System.out.println("Наибольшую последовательность Коллатца " + k + " достигается при " + max);
    }
	
    public static void main(String[] args) {
		print_collatz();
    }
}